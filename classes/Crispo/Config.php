<?php

namespace Crispo;

class Config
{

    /**
     * @property-read \PHPixie\Pixie
     */
    public $pixie;

    public function __construct($pixie)
    {
        $this->pixie = $pixie;
        $this->pixie->db->get()->execute("CREATE TABLE IF NOT EXISTS `Configs` (
			`key` VARCHAR(255) NOT NULL PRIMARY KEY,
			`value` TEXT
		)");
    }

    private function get_model($key)
    {
        $app_namespace = $this->pixie->app_namespace;
        $this->pixie->app_namespace = __NAMESPACE__ . '\\';
        $result = $this->pixie->orm->get(\Crispo\Models::Config, $key);
        $this->pixie->app_namespace = $app_namespace;
        return $result;
    }

    public function __get($key)
    {
        $model = $this->get_model($key);
        if ($model->loaded())
            return $model->value;
        return false;
    }

    public function __set($key, $value)
    {
        $model = $this->get_model($key);
        $model->value = $value;
        return $model->save();
    }

}